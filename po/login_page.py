TITLE = "Login page"

LOGIN_FIELDS = {
    "login_field_id": "LoginForm_username",
    "password_field_id": "LoginForm_password",
    "login_btn_xpath": "//input[@name='yt0']"
}
