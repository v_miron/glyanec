import pytest
from faker import Faker

from glyanec.pages.login_page import LoginPage
from glyanec.pages.home_page import HomePage


VALID_LOGIN = "mironenko"
VALID_PASSWORD = "Vova12071988"
PROFILE_MENU_CLASS = "menu"
LOGIN_URL = 'https://zakaz.glyanec.com.ua/index.php?r=site/login'


@pytest.mark.usefixtures('driver')
class TestLogin:

    @pytest.fixture(autouse=True)
    def set_up(self, driver):
        self.fake = Faker()
        self.lp = LoginPage(driver)
        self.hp = HomePage(driver)

    def test_valid_login(self):
        """Test login with valid username and password"""
        self.lp.open_login()
        self.lp.login(VALID_LOGIN, VALID_PASSWORD)
        profile_menu = self.lp.wait_for_element('class', PROFILE_MENU_CLASS)
        assert profile_menu

    def test_invalid_login(self):
        """Test login with invalid password"""
        self.lp.open_login()
        self.lp.login(VALID_LOGIN, self.fake.password())
        assert self.lp.wait_for_element('xpath', "//div[@class='loginform_title']")

    def test_login_without_password(self):
        """Test login without password"""
        self.lp.open_login()
        self.lp.login(VALID_LOGIN)
        assert self.lp.wait_for_element('xpath', "//div[@class='loginform_title']")

    def test_login_without_login(self):
        """Test login without login"""
        self.lp.open_login()
        self.lp.login('', self.fake.password())
        assert self.lp.wait_for_element('xpath', "//div[@class='loginform_title']")
