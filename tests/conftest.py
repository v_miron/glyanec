import pytest
from selenium import webdriver
from selenium.webdriver import FirefoxOptions


@pytest.fixture(scope="class")
def driver(request):
    options = FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(executable_path='C:/Users/vmiro/geckodriver.exe', options=options)
    # driver = webdriver.Firefox()
    yield driver
    driver.quit()
