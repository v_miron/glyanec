from selenium.webdriver.common.by import By

from glyanec.po.login_page import LOGIN_FIELDS
from glyanec.pages.base_page import BasePage


class LoginPage(BasePage):

    def login(self, login='', password=''):
        loc = LOGIN_FIELDS
        login_field = self.driver.find_element(By.ID, loc["login_field_id"])
        login_field.send_keys(login)
        password_field = self.driver.find_element(By.ID, loc["password_field_id"])
        password_field.send_keys(password)
        login_btn = self.driver.find_element(By.XPATH, loc["login_btn_xpath"])
        login_btn.click()
