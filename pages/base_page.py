from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class BasePage:

    base_url = "https://zakaz.glyanec.com.ua/index.php?r=site/login"

    def __init__(self, driver):
        self.driver = driver

    def open_login(self):
        self.driver.get(self.base_url)

    def by_type(self, locator_type):
        if locator_type == 'xpath':
            return By.XPATH
        elif locator_type == 'class':
            return By.CLASS_NAME
        elif locator_type == 'id':
            return By.ID
        elif locator_type == 'name':
            return By.NAME
        elif locator_type == 'link':
            return By.LINK_TEXT

    def wait_for_element(self, locator_type, locator):
        locator_type = locator_type.lower()
        wait = WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located((self.by_type(locator_type), locator))
        )
        return wait
