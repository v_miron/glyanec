from selenium.webdriver.common.by import By
import time

from glyanec.pages.base_page import BasePage


class HomePage(BasePage):

    def exit(self):
        user_menu_btn = self.wait_for_element(By.XPATH, "//a[@class='btn_popup_user_info']")
        user_menu_btn.click()
        exit_btn = self.wait_for_element(By.XPATH, "//div[@class='exit']")
        exit_btn.click()
